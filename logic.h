#ifndef LOGIC_H
#define LOGIC_H

#include <QObject>

class logic : public QObject
{
    Q_OBJECT
public:
    explicit logic(QObject *parent = 0);

signals:
    void decider(int sign,QString firstnum);
    void gettingsec(QString secondnum);

public slots:
    float settingvalue();

};

#endif // LOGIC_H
