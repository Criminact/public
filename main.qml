import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.0


Window {
    visible: true
    id: mainwindow
    width: Screen.width
    height: Screen.height
    title: "CALCI da Vinci"

    Rectangle {
        id: textrect
        width:  mainwindow.width
        height: mainwindow.height / 3
        x: 0
        y: 0
        color: "#95a5a6"

        TextInput {
            id: textinput
            maximumLength: 12
            anchors.fill: parent
            font.family: "Helvetica"
            font.pixelSize: 20
            validator: IntValidator{} //this forces the TextInput to take only integers

            Rectangle {
                id: clearbuttonrect
                width: mainwindow.width / 4
                height: (2*(mainwindow.height)/3) / 4
                x: (3*mainwindow.width) / 4
                y: mainwindow.height / 5

                Button {
                    id: clearbutton
                    anchors.fill: parent
                    Text {
                        id: clearbuttontext
                        text: "C"
                        font.family: "Helvetica"
                        font.pixelSize: 20
                        font.bold: true
                        anchors.centerIn: parent
                    }
                    onClicked:{
                    textinput.clear()
                        signinput.clear()
                    }
                }

            }
        }


    }

    Rectangle {
        id: signrect
        width:  mainwindow.width / 4
        height: mainwindow.height / 5
        x: (3*mainwindow.width) / 4
        y: 0
        color: "#95a5a6"
        TextInput {
            id: signinput
            anchors.fill: parent
            font.family: "Helvetica"
            font.pixelSize: 20
            anchors.centerIn: parent
        }

    }

    Rectangle {
        id: buttonsrect
        width: mainwindow.width
        height: 2*(mainwindow.height) / 3
        x: 0
        y: mainwindow.height / 3

        Rectangle {
            id: onebuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 0
            y: 0
            color: "blue"
            Button {
                id: onebutton
                anchors.fill: parent
                Text {
                    id: onebuttontext
                    text: "1"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
            onClicked:{
            textinput.text = textinput.text + "1"
            }

            }

        }

        Rectangle {
            id: twobuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 4
            y: 0

            Button {
                id: twobutton
                anchors.fill: parent
                Text {
                    id: twobuttontext
                    text: "2"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "2"
                }
            }

        }

        Rectangle {
            id: threebuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 2
            y: 0

            Button {
                id: threebutton
                anchors.fill: parent
                Text {
                    id: threebuttontext
                    text: "3"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "3"
                }
            }

        }
        Rectangle {
            id: plusbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 3*(mainwindow.width) / 4
            y: 0

            Button {
                id: plusbutton
                anchors.fill: parent
                Text {
                    id: plusbuttontext
                    text: "+"
                    font.family: "Verdana"
                    font.pixelSize: 24
                    anchors.centerIn: parent
                }
                onClicked:{
                    logic.decider(1,textinput.text);
                signinput.text =  "+"
                    textinput.clear()
                }
            }

        }

        Rectangle {
            id: fourbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 0
            y: buttonsrect.height / 4

            Button {
                id: fourbutton
                anchors.fill: parent
                Text {
                    id: fourbuttontext
                    text: "4"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "4"
                }
            }

        }

        Rectangle {
            id: fivebuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 4
            y: buttonsrect.height / 4

            Button {
                id: fivebutton
                anchors.fill: parent
                Text {
                    id: fivebuttontext
                    text: "5"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "5"
                }
            }

        }

        Rectangle {
            id: sixbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 2
            y: buttonsrect.height / 4

            Button {
                id: sixbutton
                anchors.fill: parent
                Text {
                    id: sixbuttontext
                    text: "6"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "6"
                }
            }

        }

        Rectangle {
            id: minusbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 3*(mainwindow.width) / 4
            y: buttonsrect.height / 4

            Button {
                id: minusbutton
                anchors.fill: parent
                Text {
                    id: minusbuttontext
                    text: "-"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                    logic.decider(2,textinput.text);
                signinput.text =  "-"
                    textinput.clear()
                }
            }

        }

        Rectangle {
            id: sevenbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 0
            y: buttonsrect.height / 2

            Button {
                id: sevenbutton
                anchors.fill: parent
                Text {
                    id: sevenbuttontext
                    text: "7"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "7"
                }
            }

        }

        Rectangle {
            id: eightbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 4
            y: buttonsrect.height / 2

            Button {
                id: eightbutton
                anchors.fill: parent
                Text {
                    id: eightbuttontext
                    text: "8"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "8"
                }
            }

        }
        Rectangle {
            id: ninebuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 2
            y: buttonsrect.height / 2

            Button {
                id: ninebutton
                anchors.fill: parent
                Text {
                    id: ninebuttontext
                    text: "9"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "9"
                }
            }

        }

        Rectangle {
            id: mulbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 3*(mainwindow.width) / 4
            y: buttonsrect.height / 2

            Button {
                id: mulbutton
                anchors.fill: parent
                Text {
                    id: mulbttontext
                    text: "X"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                    logic.decider(3,textinput.text);
                signinput.text =  "x"
                    textinput.clear()
                }
            }

        }

        Rectangle {
            id: decimalbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 0
            y: 3*(buttonsrect.height) / 4

            Button {
                id: decimalbutton
                anchors.fill: parent
                Text {
                    id: decimalbuttontext
                    text: "."
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "."
                }
            }

        }

        Rectangle {
            id: zerobuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 4
            y: 3*(buttonsrect.height) / 4

            Button {
                id: zerobutton
                anchors.fill: parent
                Text {
                    id: zerobuttontext
                    text: "0"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                textinput.text = textinput.text + "0"
                }
            }

        }

        Rectangle {
            id: equalbuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: mainwindow.width / 2
            y: 3*(buttonsrect.height) / 4

            Button {
                id: equalbutton
                anchors.fill: parent
                Text {
                    id: equalbuttontext
                    text: "="
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                    logic.gettingsec(textinput.text);
                    textinput.clear()
                    signinput.clear()
                signinput.text = "="
                    textinput.text = logic.settingvalue();
                }
            }

        }

        Rectangle {
            id: dividebuttonrect
            width: mainwindow.width / 4
            height: (2*(mainwindow.height)/3) / 4
            x: 3*(mainwindow.width) / 4
            y: 3*(buttonsrect.height) / 4

            Button {
                id: dividebutton
                anchors.fill: parent
                Text {
                    id: dividebuttontext
                    text: "/"
                    font.family: "Helvetica"
                    font.pixelSize: 14
                    font.bold: true
                    anchors.centerIn: parent
                }
                onClicked:{
                    logic.decider(4,textinput.text);
                signinput.text =  "x"
                    textinput.clear()
                }

            }

        }

    }
}
