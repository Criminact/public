#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

vector<vector<Point> >contours;
vector<Vec4i>hercy; 
vector<Point2f>man(0);

int main(int, char**)
{
VideoCapture cap(0); // open the default camera
//Video Capture cap(path_to_video); // open the video file
if(!cap.isOpened())  // check if we succeeded
    return -1;

for(;;)
{
    Mat frame1,frame2 = imread("frame.jpg",CV_LOAD_IMAGE_GRAYSCALE),frame3,frame4,frame5;
  	cap >> frame1;

  	if(waitKey(100) == 'r')
  	{
  	imwrite("frame.jpg",frame1);
  	}
  	cvtColor(frame1,frame1,CV_BGR2GRAY);
  	absdiff(frame1,frame2,frame3);
  	imshow("GRAY AND ABSDIFF",frame3);

  	threshold(frame3,frame3,60,255,CV_THRESH_BINARY);
  	imshow("THRESHOLDED",frame3);
//noise removal needed lets check for the medianBlur
  	medianBlur(frame3,frame3,15);
  	imshow("Noise removed",frame3);

  	equalizeHist(frame3,frame3);
  	imshow("histogram",frame3);

  	Canny(frame3,frame5,50,150,3);
  	imshow("Canny edge detect",frame5);

 findContours(frame3,contours,hercy,RETR_EXTERNAL,CHAIN_APPROX_NONE);
 int largest_area=0;
 int largest_contour_index=0;
 Rect bounding_rect;
 for(int i=0;i< contours.size();i++)
 {
 	double a=contourArea(contours[i],false);
 	if(a > largest_area)
 	{
 		largest_area=a;
 		cout << i << "area" << a << endl;
 		largest_contour_index=i;
 		bounding_rect=boundingRect(contours[i]);
 	}
 }
 Scalar color(255,0,255);
 drawContours(frame1,contours,largest_contour_index,color,CV_FILLED,8,hercy);
 rectangle(frame1,bounding_rect,Scalar(255,0,255),2,8,0);
    imshow("contours",frame1);

for(int i=0;i<frame3.rows;i++){
	for(int j=0;j<frame3.cols;j++){
		
		if(frame3.at<Vec3b>(i,j)[0] > 240)
			{
				man.push_back(Point(i,j));
				//cout << Point(i,j);
			}
	}
}
cout << man[0] << ',' << man[sizeof(man)]  << endl;
/*
Point p1(man[0]);
Point p2(man[sizeof(man)]);
*/
for(int i=0;i<frame5.rows;i++){
	for(int j=0;j<frame5.cols;j++){
		
		if(frame5.at<Vec3b>(i,j)[0] > 230)
			{
				man.push_back(Point(i,j));
				//cout << Point(i,j);
			}
	}
}

man.clear();

  	if(waitKey(30) >= 0) break;
}

return 0;
}
